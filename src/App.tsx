import React, { useState, useEffect } from "react";
import "./App.css";
import AppFrom from "./components/AppForm";
import VideoPannel from "./components/VidePannel";
import VideoSerice, { devices } from "./lib/videoService";
import AdvancedForm from "./components/AdvancedFrom";
import Notification from "./components/Notification";
import { Layout, Row, Col } from "antd";
import { IconType } from "antd/lib/notification";

let vs: VideoSerice;

function App() {
  const [appId, setAppId] = useState("");
  const [token, setToken] = useState("");
  const [channel, setChannel] = useState<string>("");
  const [uid, setUid] = useState<string | number | undefined>(undefined);
  const [mode, setMode] = useState("live");
  const [codec, setCodec] = useState("h264");
  const [microphoneId, setMicrophoneId] = useState("");
  const [cameraId, setCameraId] = useState("");

  const startPannel: number[] = [];
  const [pannel, setPannel] = useState(startPannel);

  const [localPannel, setLocaPannel] = useState("");

  const [videos, setVideos] = useState<devices["videos"]>([]);
  const [audios, setAudios] = useState<devices["audios"]>([]);

  const [message, setMessage] = useState<{
    type: IconType | undefined;
    message: string | undefined;
    flag: number;
  }>({ type: undefined, message: undefined, flag: 0 });
  useEffect(() => {
    const getDevices = async () => {
      try {
        const devices = await VideoSerice.getDevices();
        setVideos(devices.videos);
        setAudios(devices.audios);
        setCameraId(devices.audios.length > 1 ? devices.videos[0].value : "");
        setMicrophoneId(
          devices.audios.length > 1 ? devices.audios[0].value : ""
        );
      } catch (err) {
        console.error(err);
      }
    };
    getDevices();
  }, []);

  useEffect(() => {
    vs = new VideoSerice({
      mode,
      codec,
      appId,
      channel,
      token,
      uid,
      microphoneId,
      cameraId,
    });
  }, [mode, codec, appId, channel, token, uid, microphoneId, cameraId]);

  const onRemove = (id: number) => {
    const newPannel = pannel.filter((each) => {
      if (each !== id) {
        return each;
      }
    });
    setPannel(newPannel);
  };

  const onAdd = (id: number) => {
    setPannel([...pannel, id]);
  };

  const onMessage = (message: string) => {
    console.log("app message", message);
    setMessage({
      type: "info",
      message: message,
      flag: new Date().getTime(),
    });
  };

  const onJoin = async () => {
    try {
      await vs.join(onRemove, onAdd, onMessage);
      setMessage({
        type: "success",
        message: "joined",
        flag: new Date().getTime(),
      });
      setLocaPannel("local");
    } catch (error) {
      setMessage({
        type: "error",
        message: error.message,
        flag: new Date().getTime(),
      });
      console.error(error);
    }
  };

  const onLeave = async () => {
    try {
      await vs.leave();
      setLocaPannel("");
      setPannel([]);
      setMessage({
        type: "success",
        message: "leaved",
        flag: new Date().getTime(),
      });
    } catch (error) {
      setMessage({
        type: "error",
        message: error,
        flag: new Date().getTime(),
      });
      console.error(error);
    }
  };

  const onPublish = async () => {
    try {
      await vs.publish();
      setMessage({
        type: "success",
        message: "published",
        flag: new Date().getTime(),
      });
    } catch (error) {
      setMessage({
        type: "error",
        message: error,
        flag: new Date().getTime(),
      });
      console.error(error);
    }
  };

  const onUnpublish = async () => {
    try {
      await vs.unpublish();
      setMessage({
        type: "success",
        message: "unpublished",
        flag: new Date().getTime(),
      });
    } catch (error) {
      setMessage({
        type: "error",
        message: error,
        flag: new Date().getTime(),
      });
      console.error(error);
    }
  };

  const { Header } = Layout;
  return (
    <Layout>
      <Header className="header">Code-Test</Header>
      <Layout style={{ height: "100%" }}>
        <Row>
          <Notification
            message={message.message}
            type={message.type}
            flag={message.flag}
          />
          <Col span={8} style={{ minWidth: "440px" }}>
            <AppFrom
              vs={vs}
              appId={appId}
              token={token}
              channel={channel}
              setAppId={setAppId}
              setToken={setToken}
              setChannel={setChannel}
              onJoin={onJoin}
              onLeave={onLeave}
              onPublish={onPublish}
              onUnpublish={onUnpublish}
            />
            <AdvancedForm
              uid={uid}
              mode={mode}
              codec={codec}
              cameras={videos}
              microphones={audios}
              camera={cameraId}
              microphone={microphoneId}
              cameraResolution={""}
              setUid={setUid}
              setMode={setMode}
              setCamera={setCameraId}
              setMic={setMicrophoneId}
              setCodec={setCodec}
            />
          </Col>
          <Col span={16}>
            <VideoPannel vs={vs} localPannel={localPannel} pannel={pannel} />
          </Col>
        </Row>
      </Layout>
    </Layout>
  );
}

export default App;
