import React from "react";
import { Card, Input, Select, Radio } from "antd";
import { item } from "../lib/videoService";

const { Option } = Select;

type Props = {
  uid: string | number | undefined;
  camera: string;
  microphone: string;
  cameraResolution: string;
  mode: string;
  codec: string;
  cameras: item[];
  microphones: item[];
  setUid: (uid: string) => void;
  setCamera: (camera: string) => void;
  setMic: (mic: string) => void;
  setMode: (mode: string) => void;
  setCodec: (codec: string) => void;
};
const AdvancedForm = (props: Props) => {
  const {
    uid,
    cameras,
    microphones,
    camera,
    microphone,
    mode,
    codec,
    setUid,
    setCamera,
    setMic,
    setMode,
    setCodec,
  } = props;
  return (
    <Card style={{ marginTop: "20px" }}>
      <Input
        placeholder="uid"
        style={{ marginBottom: "5px" }}
        type="text"
        value={uid}
        onChange={(e) => setUid(e.target.value)}
      />
      <Select
        value={camera}
        style={{ width: "100%", marginBottom: "5px" }}
        onChange={setCamera}
      >
        {cameras.map((each) => {
          return (
            <Option key={each.value} value={each.value}>
              {each.name}
            </Option>
          );
        })}
      </Select>
      <Select
        style={{ width: "100%", marginBottom: "5px" }}
        value={microphone}
        onChange={setMic}
      >
        {microphones.map((each) => (
          <Option key={each.value} value={each.value}>
            {each.name}
          </Option>
        ))}
      </Select>
      <div>
        Mode:
        <Radio.Group value={mode} onChange={(e) => setMode(e.target.value)}>
          <Radio value={"live"}>live</Radio>
          <Radio value={"rtc"}>rtc</Radio>
        </Radio.Group>
      </div>

      <div>
        Codec:
        <Radio.Group value={codec} onChange={(e) => setCodec(e.target.value)}>
          <Radio value={"h264"}>h264</Radio>
          <Radio value={"vp8"}>vp8</Radio>
        </Radio.Group>
      </div>
    </Card>
  );
};

export default AdvancedForm;
