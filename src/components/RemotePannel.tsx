import React, { useEffect } from "react";
import VideoSerice from "../lib/videoService";

type Props = {
  vs: VideoSerice;
  pannelId: number;
};
const RemotePannel = (props: Props) => {
  useEffect(() => {
    const rs = props.vs.rtc.remoteStreams.find(
      (each) => props.pannelId === each.getId()
    );
    rs.play(`remote_pannel_${props.pannelId}`);
  }, []);
  return (
    <div
      id={`remote_pannel_${props.pannelId}`}
      style={{ width: "480px", height: "360px" }}
    ></div>
  );
};

export default RemotePannel;
