import React, { useState } from "react";
import { Card, Input, Button } from "antd";
import VideoSerice from "../lib/videoService";

type Props = {
  vs: VideoSerice;
  appId: string;
  token: string;
  channel: string;
  setAppId: (value: string) => void;
  setToken: (value: string) => void;
  setChannel: (value: string) => void;
  onJoin: () => void;
  onLeave: () => void;
  onPublish: () => void;
  onUnpublish: () => void;
};
const AppForm = (props: Props) => {
  const {
    setAppId,
    setToken,
    setChannel,
    appId,
    token,
    channel,
    onJoin,
    onLeave,
    onPublish,
    onUnpublish,
  } = props;

  return (
    <Card>
      <Input
        placeholder="appId"
        style={{ marginTop: "5px" }}
        type="text"
        value={appId}
        onChange={(e) => setAppId(e.target.value)}
      />
      <Input
        placeholder="token"
        style={{ marginTop: "5px" }}
        type="text"
        value={token}
        onChange={(e) => setToken(e.target.value)}
      />
      <Input
        placeholder="channel"
        style={{ marginTop: "5px" }}
        type="text"
        value={channel}
        onChange={(e) => setChannel(e.target.value)}
      />
      <div style={{ marginTop: "5px" }}>
        <Button style={{ marginLeft: "5px" }} onClick={onJoin}>
          join
        </Button>
        <Button style={{ marginLeft: "5px" }} onClick={onLeave}>
          leave
        </Button>
        <Button style={{ marginLeft: "5px" }} onClick={onPublish}>
          publish
        </Button>
        <Button style={{ marginLeft: "5px" }} onClick={onUnpublish}>
          unpublish
        </Button>
      </div>
    </Card>
  );
};

export default AppForm;
