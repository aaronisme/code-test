import React, { useEffect } from "react";
import { notification, message, Divider } from "antd";
import { IconType } from "antd/lib/notification";
import { type } from "os";

type Props = {
  type: IconType | undefined;
  message: string | undefined;
  flag: number;
};

const Notification = (props: Props) => {
  useEffect(() => {
    if (props.type) {
      notification[props.type]({
        message: props.message,
      });
    }
  }, [props.flag]);
  return <React.Fragment />;
};

export default Notification;
