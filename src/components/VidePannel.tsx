import React, { useEffect } from "react";
import VideoSerice from "../lib/videoService";
import RemotePannel from "./RemotePannel";

type Props = {
  vs: VideoSerice;
  localPannel: string;
  pannel: number[];
};

const VideoPannel = (props: Props) => {
  useEffect(() => {
    if (props.localPannel === "local") {
      props.vs.rtc.localStream.play("local_stream");
    }
  }, [props.localPannel]);
  return (
    <div>
      <div id="local_stream" style={{ width: "480px", height: "360px" }} />
      <div>
        {props.pannel.map((each) => (
          <RemotePannel vs={props.vs} pannelId={each} />
        ))}
      </div>
    </div>
  );
};

export default VideoPannel;
