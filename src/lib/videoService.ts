interface rtc {
  client: any;
  joined: boolean;
  published: boolean;
  localStream: any | null;
  remoteStreams: any[];
  params: any;
}

export type item = {
  name: string;
  kind: string;
  value: string;
};

export type devices = {
  videos: item[];
  audios: item[];
};

type rtcOption = {
  mode: string;
  codec: string;
  appId: string;
  channel: string;
  token: string;
  uid: string | number | undefined;
  microphoneId: string;
  cameraId: string;
};

export default class VideoSerice {
  public rtc: rtc;
  private rtcOption: rtcOption;
  constructor(rtcOption: rtcOption) {
    this.rtcOption = rtcOption;
    this.rtc = {
      //@ts-ignore
      client: window.AgoraRTC.createClient({
        mode: rtcOption.mode,
        codec: rtcOption.codec,
      }),
      joined: false,
      published: false,
      localStream: null,
      remoteStreams: [],
      params: {},
    };
  }

  public static async getDevices(): Promise<devices> {
    return new Promise((resolve, reject) => {
      const devices: devices = {
        videos: [],
        audios: [],
      };
      //@ts-ignore
      window.AgoraRTC.getDevices(
        function (items: any[]) {
          items
            .filter(function (item) {
              return ["audioinput", "videoinput"].indexOf(item.kind) !== -1;
            })
            .map(function (item) {
              return {
                name: item.label,
                value: item.deviceId,
                kind: item.kind,
              };
            });
          for (let i = 0; i < items.length; i++) {
            const item = items[i];
            if ("videoinput" == item.kind) {
              let name = item.label;
              const value = item.deviceId;
              if (!name) {
                name = "camera-" + devices.videos.length;
              }
              devices.videos.push({
                name: name,
                value: value,
                kind: item.kind,
              });
            }
            if ("audioinput" == item.kind) {
              let name = item.label;
              const value = item.deviceId;
              if (!name) {
                name = "microphone-" + devices.audios.length;
              }
              devices.audios.push({
                name: name,
                value: value,
                kind: item.kind,
              });
            }
          }
          resolve(devices);
        },
        function (err: any) {
          reject(err);
        }
      );
    });
  }

  public async join(
    onRemove: (id: number) => void,
    onAdd: (id: number) => void,
    onMessage: (message: string) => void
  ) {
    const {
      appId,
      token,
      channel,
      uid,
      microphoneId,
      cameraId,
    } = this.rtcOption;
    if (this.rtc.joined) {
      throw new Error("you already joined");
    }
    if (appId.length == 0 || channel.length == 0 || token.length == 0) {
      throw new Error("please check your data");
    }
    await this.init(appId);
    this.registerRtcEvent(onRemove, onAdd, onMessage);

    await this.joinChannel(token, channel, uid);

    await this.createLocalStream(microphoneId, cameraId);
    await this.publish();
  }

  public async publish() {
    const rtc = this.rtc;
    return new Promise((resolve, reject) => {
      if (!rtc.client) {
        reject("Please Join Room First");
      }
      if (rtc.published) {
        reject("Your already published");
      }
      const oldState = rtc.published;

      // publish localStream
      rtc.client.publish(rtc.localStream, function (err: any) {
        rtc.published = oldState;
        reject("publish failed");
      });
      rtc.published = true;
      resolve("published");
    });
  }

  public async unpublish() {
    const rtc = this.rtc;
    return new Promise((resolve, reject) => {
      if (!rtc.client) {
        reject("Please Join Room First");
      }
      if (!rtc.published) {
        reject("Your didn't publish");
      }
      const oldState = rtc.published;
      rtc.client.unpublish(rtc.localStream, function (err: any) {
        rtc.published = oldState;
        reject(err);
        console.log("unpublish failed");
        console.error(err);
      });
      rtc.published = false;
      resolve("unpublish");
    });
  }

  public async leave() {
    const rtc = this.rtc;
    return new Promise((resolve, reject) => {
      if (!rtc.client) {
        reject("Please Join First");
      }
      if (!rtc.joined) {
        reject("You are not in channel");
      }

      rtc.client.leave(
        function () {
          if (rtc.localStream.isPlaying()) {
            rtc.localStream.stop();
          }
          rtc.localStream.close();
          for (let i = 0; i < rtc.remoteStreams.length; i++) {
            const stream = rtc.remoteStreams.shift();
            if (stream.isPlaying()) {
              stream.stop();
            }
          }
          rtc.localStream = null;
          rtc.remoteStreams = [];
          console.log("client leaves channel success");
          rtc.published = false;
          rtc.joined = false;
          resolve("leaval success");
        },
        function (err: any) {
          console.log("channel leave failed");
          reject(err);
          console.error(err);
        }
      );
    });
  }

  private registerRtcEvent(
    onRemove: (id: number) => void,
    onAdd: (id: number) => void,
    onMessage: (message: string) => void
  ) {
    // Occurs when an error message is reported and requires error handling.
    const rtc = this.rtc;
    rtc.client.on("error", (err: any) => {
      console.log(err);
    });
    // Occurs when the peer user leaves the channel; for example, the peer user calls Client.leave.

    this.rtc.client.on("peer-leave", function (evt: any) {
      const id = evt.uid;
      console.log("id", evt);
      const streams = rtc.remoteStreams.filter((e) => id !== e.getId());
      const peerStream = rtc.remoteStreams.find((e) => id === e.getId());
      if (peerStream && peerStream.isPlaying()) {
        peerStream.stop();
      }
      rtc.remoteStreams = streams;
      if (id !== rtc.params.uid) {
        onRemove(id);
        onMessage("peer leave");
        console.log("remove view");
      }
      console.log("peer-leave", id);
    });
    // Occurs when the local stream is published.
    rtc.client.on("stream-published", function (evt: any) {
      onMessage("peer leave");
      console.log("stream-published");
    });
    // Occurs when the remote stream is added.
    rtc.client.on("stream-added", function (evt: any) {
      const remoteStream = evt.stream;
      const id = remoteStream.getId();
      onMessage("stream-added uid: " + id);
      if (id !== rtc.params.uid) {
        rtc.client.subscribe(remoteStream, function (err: any) {
          console.log("stream subscribe failed", err);
        });
      }
      console.log("stream-added remote-uid: ", id);
    });
    // Occurs when a user subscribes to a remote stream.
    rtc.client.on("stream-subscribed", function (evt: any) {
      const remoteStream = evt.stream;
      const id = remoteStream.getId();
      rtc.remoteStreams.push(remoteStream);
      // Todo: add view here
      // addView(id)
      onAdd(id);
      remoteStream.play("remote_video_" + id);
      onMessage("stream-subscribed remote-uid: " + id);
      console.log("stream-subscribed remote-uid: ", id);
    });
    // Occurs when the remote stream is removed; for example, a peer user calls Client.unpublish.
    rtc.client.on("stream-removed", function (evt: any) {
      const remoteStream = evt.stream;
      const id = remoteStream.getId();
      if (remoteStream.isPlaying()) {
        remoteStream.stop();
      }
      rtc.remoteStreams = rtc.remoteStreams.filter(function (stream) {
        return stream.getId() !== id;
      });
      // event
      // removeView(id)
      onRemove(id);
      onMessage("stream-subscribed remote-uid: " + id);
      console.log("stream-removed remote-uid: ", id);
    });
    rtc.client.on("onTokenPrivilegeWillExpire", function () {
      // After requesting a new token
      // rtc.client.renewToken(token);
      onMessage("onTokenPrivilegeWillExpire");
      console.log("onTokenPrivilegeWillExpire");
    });
    rtc.client.on("onTokenPrivilegeDidExpire", function () {
      // Toast.info("onTokenPrivilegeDidExpire")
      console.log("onTokenPrivilegeDidExpire");
    });
  }

  private async init(appId: string) {
    const rtc = this.rtc;
    return new Promise((resolve, reject) => {
      rtc.client.init(
        appId,
        function () {
          resolve("inited");
        },
        function (err: any) {
          reject(err);
        }
      );
    });
  }

  private async joinChannel(
    token: string | null,
    channel: string,
    uid: string | number | undefined
  ) {
    const rtc = this.rtc;
    return new Promise((resolve, reject) => {
      rtc.client.join(
        token,
        channel,
        uid,
        function () {
          rtc.joined = true;
          resolve("joined");
        },
        function (err: any) {
          reject(err);
          console.error("client join failed", err);
        }
      );
    });
  }

  private async createLocalStream(microphoneId: string, cameraId: string) {
    const rtc = this.rtc;
    return new Promise((resolve, reject) => {
      //@ts-ignore
      rtc.localStream = window.AgoraRTC.createStream({
        streamID: rtc.params.uid,
        audio: true,
        video: true,
        screen: false,
        microphoneId: microphoneId,
        cameraId: cameraId,
      });

      rtc.localStream.init(
        function () {
          resolve("published");
        },
        function (err: any) {
          reject(err);
        }
      );
    });
  }
}
