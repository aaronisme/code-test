import VideoService from "../videoService";

describe("VideoSerice", () => {
  it("should return devives if get devices right", async () => {
    const items = [
      {
        label: "test-audio",
        kind: "audioinput",
        deviceId: "test-deviceId",
      },
      {
        label: "test-video",
        kind: "videoinput",
        deviceId: "test-video-deviceId",
      },
    ];
    // @ts-ignore
    window.AgoraRTC = {
      getDevices: (
        onSuccess: (items: any[]) => void,
        onFail: (err: any) => void
      ) => {
        onSuccess(items);
      },
    };

    const devices = await VideoService.getDevices();
    expect(devices).toEqual({
      audios: [
        { kind: "audioinput", name: "test-audio", value: "test-deviceId" },
      ],
      videos: [
        {
          kind: "videoinput",
          name: "test-video",
          value: "test-video-deviceId",
        },
      ],
    });
  });

  it("should reject promise if get devices not success", async () => {
    // @ts-ignore
    window.AgoraRTC = {
      getDevices: (
        onSuccess: (items: any[]) => void,
        onFail: (err: any) => void
      ) => {
        onFail("failed");
      },
    };

    try {
      await expect(VideoService.getDevices());
    } catch (err) {
      expect(err).toBe("failed");
    }
  });
});
